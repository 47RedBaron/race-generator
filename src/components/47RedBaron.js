'use strict'

/**
 * @author 47RedBaron
 * import RedBaronModule from './47RedBaron.js'
 * const gdy = new RedBaronModule()
 */

function gdyModule () {
    /**
   * @description MATHS
   */
  this.averageHexadecimal = function (color1, color2, percent) {
    const hexToRgb = hex =>
      hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, (m, r, g, b) => '#' + r + r + g + g + b + b)
         .substring(1).match(/.{2}/g)
         .map(x => parseInt(x, 16))
    const rgbToHex = (r, g, b) => '#' + [r, g, b]
     .map(x => x.toString(16).padStart(2, '0')).join('')

    // Convert Hexadecimal => RGB
    let colorsToRgb = [hexToRgb(color1), hexToRgb(color2)]
    // Result to be store into this varable
    var colorAverage = [0, 0, 0]

    colorAverage.forEach((value, i) => {
      colorAverage[i] = parseInt((colorsToRgb[1][i] - colorsToRgb[0][i]) * percent / 100 + colorsToRgb[0][i])
    })

    return rgbToHex(colorAverage[0], colorAverage[1], colorAverage[2])
  }
  this.getRandomNumber = function (number) {
    return Math.floor(Math.random() * ((number + 1) - 1)) + 1
  }
  /**
   * @description WORDINGS
   */
  this.daySuffix = function (day) {
    switch (day) {
      case 1:
      case 21:
      case 31:
        return 'st'
      case 2:
      case 22:
        return 'nd'
      case 3:
      case 23:
        return 'rd'
      default:
        return 'th'
    }
  }
  this.toPropercase = function (str) {
    return str.toLowerCase().split(' ').map(function (word) {
      return (word.charAt(0).toUpperCase() + word.slice(1))
    }).join(' ')
  }
  /**
   * @description FRONT ELEMENTS
   */
  this.progressBar = function (colorStart, colorEnd) {
    /**
     * CSS
      .progressBar {
        left: 2%;
        border-radius: 4px 4px;
        width:96%;
        height: 4px;
        position: fixed;
        top: 0%;
        z-index:1;
      }
     */

    var h = document.documentElement
    var b = document.body
    var st = 'scrollTop'
    var sh = 'scrollHeight'
    var progress = document.querySelector('.progressBar')
    var self = this

    document.addEventListener('scroll', function () {
      var scroll = (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) * 100

      progress.style.setProperty('background', 'linear-gradient(to right,' + colorStart + ', ' + self.averageHexadecimal(colorStart, colorEnd, scroll) + ' ' + scroll + '%' + ', transparent 0)')
    })
  }
  /**
   * @description STUFFS
   */
  this.log = function () {
    console.log('gdy.log() is alive!')
  }
}
export default gdyModule
